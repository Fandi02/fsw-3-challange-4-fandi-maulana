// console.log("Implement servermu disini yak 😝!");
// /**
//  * Impor HTTP Standar Library dari Node.js
//  * Hal inilah yang nantinya akan kita gunakan untuk membuat
//  * HTTP Server
//  * */
// const http = require('http');
// const { PORT = 8000 } = process.env; // Ambil port dari environment variable

// const fs = require('fs');
// const path = require('path');
// const PUBLIC_DIRECTORY = path.join(__dirname, '../public'); 

// function getHTML(htmlFileName) {
//   const htmlFilePath = path.join(PUBLIC_DIRECTORY, htmlFileName);
//   return fs.readFileSync(htmlFilePath, 'utf-8')
// }

// function onRequest(req, res) {
//   switch(req.url) {
//     case "/":
//       res.writeHead(200)
//       res.end(getHTML("index.html"))
//       return;
//     case "/cars":
//       res.writeHead(200)
//       res.end(getHTML("cars.html"))
//       return;
//     case "/css":
//       res.writeHead(200)
//       Response.write(fs.readFileSync(path.join(PUBLIC_DIRECTORY, "css/style.css")))
//       res.end()
//     default:
//       res.writeHead(404)
//       res.end(getHTML("404.html"))
//       return;
//   }
// }

// const server = http.createServer(onRequest);

// // Jalankan server
// server.listen(PORT, 'localhost', () => {
//   console.log("Server sudah berjalan, silahkan buka http://localhost:%d", PORT);
// })

// HTTP Module for Creating Server and Serving Static Files Using Node.js
// Static Files: HTML, CSS, JS, Images
// Get Complete Source Code from Pabbly.com

var http = require('http');
var fs = require('fs');
var path = require('path');
const PUBLIC_DIRECTORY = path.join(__dirname, '../public');

http.createServer(function(req, res){

    if(req.url === "/"){
        fs.readFile(PUBLIC_DIRECTORY + "/index.html", "UTF-8", function(err, html){
            res.writeHead(200, {"Content-Type": "text/html"});
            res.end(html);
        });
    }else if(req.url === "/cars"){
      fs.readFile(PUBLIC_DIRECTORY + "/cars.html", "UTF-8", function(err, html){
          res.writeHead(200, {"Content-Type": "text/html"});
          res.end(html);
      });
    }else if(req.url.match("\.css$")){
        var cssPath = path.join(__dirname, '../public', req.url);
        var fileStream = fs.createReadStream(cssPath, "UTF-8");
        res.writeHead(200, {"Content-Type": "text/css"});
        fileStream.pipe(res);
    }else if(req.url.match("\.js$")){
        var cssPath = path.join(__dirname, '../public', req.url);
        var fileStream = fs.createReadStream(cssPath, "UTF-8");
        res.writeHead(200, {"Content-Type": "text/js"});
        fileStream.pipe(res);
    }else if(req.url.match("\.png$")){
        var imagePath = path.join(__dirname, '../public', req.url);
        var fileStream = fs.createReadStream(imagePath);
        res.writeHead(200, {"Content-Type": "image/png"});
        fileStream.pipe(res);
    }else if(req.url.match("\.jpg$")){
        var imagePath = path.join(__dirname, '../public', req.url);
        var fileStream = fs.createReadStream(imagePath);
        res.writeHead(200, {"Content-Type": "image/jpg"});
        fileStream.pipe(res);
    }else{
        fs.readFile(PUBLIC_DIRECTORY + "/404.html", "UTF-8", function(err, html){
            res.writeHead(404, {"Content-Type": "text/html"});
            res.end(html);
        });
    }

}).listen(8000, 'localhost', () => {
    console.log("Server sudah berjalan, silahkan buka http://localhost:%d", 8000);
  })